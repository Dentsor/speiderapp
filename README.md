# Speiderapp

Speiderapp er et prosjekt hvor vi ønsker å lage mobil-applikasjoner for Norkse Speidere.

Prosjektet er per dags dato ledet av KFUK-KFUM-speidere, 
og sikter seg derfor hovedsakelig inn mot KM-speidere, 
men det er et håp om at med tid kunne samarbeide også med NSF-speidere for å gi et best mulig tilbud til flest mulig.

EDIT: Vi har fått hint om at det også er interesse fra NSF speidere, så implementasjon av multi-forbund er i gang!

## Operativ System

Den ene applikasjonen som er i utvikling per dags dato er kun tilgjengelig på android.

## Last ned

Gå til [prosjekt-siden](http://www.speiderapp.no).
