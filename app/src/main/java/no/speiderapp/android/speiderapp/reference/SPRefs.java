package no.speiderapp.android.speiderapp.reference;

public class SPRefs {
    public static final String ACTIVE_PROFILE_KEY = "active_profile";
    public static final int ACTIVE_PROFILE_DEFAULT = 1;

    public static final String AUTO_LAUNCH_KEY = "auto_launch";
    public static final int AUTO_LAUNCH_DEFAULT = 0;

    public static final String PROFILE_ASSOCIATION_KEY = "profile_association";
    public static final String PROFILE_ASSOCIATION_DEFAULT = "1";

    public static final String PROFILE_AUDIENCE_KEY = "profile_audience";
    public static final String PROFILE_AUDIENCE_DEFAULT = "1";

    public static final String IMPORT_FINISHED_KEY = "import_finished";
    public static final boolean IMPORT_FINISHED_DEFAULT = false;
}
