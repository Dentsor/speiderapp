package no.speiderapp.android.speiderapp.utilities;
/**
 * Events for blocking runnable executing on UI thread
 *
 * @author http://stackoverflow.com/questions/5996885/how-to-wait-for-android-runonuithread-to-be-finished
 *
 */
public interface BlockingOnUIRunnableListener
{

    /**
     * Code to execute on UI thread
     */
    public void onRunOnUIThread();
}