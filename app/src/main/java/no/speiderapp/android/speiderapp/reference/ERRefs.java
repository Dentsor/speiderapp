package no.speiderapp.android.speiderapp.reference;

public class ERRefs {
    public static final String[] ID_001 = {"WIP#001", "This feature is still in development!", "Events page is WIP"};
    public static final String[] ID_002 = {"ERR#002", "NothingSelected", "ActivitySettingsOld SpinnerTier NothingSelected"};
    public static final String[] ID_003 = {"WIP#003", "This is a WIP feature", "ActivitySettingsOld SpinnerUsers This is a WIP feature"};
    public static final String[] ID_004 = {"WIP#004", "This feature is still in development!", "More page is WIP"};
    public static final String[] ID_005 = {"ERR#005", "NothingSelected", "ActivityProfile SpinnerProfiles NothingSelected"};
    public static final String[] ID_006 = {"WIP#006", "Settings is still WIP!", "ActivitySettingsOld OnLoad"};
    public static final String[] ID_007 = {"ERR#007", "NothingSelected", "ActivitySettingsOld SpinnerProfiles NothingSelected"};
    public static final String[] ID_008 = {"ERR#008", "NothingSelected", "Spinner NothingSelected"};
}
