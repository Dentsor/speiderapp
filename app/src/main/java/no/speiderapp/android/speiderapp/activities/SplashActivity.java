package no.speiderapp.android.speiderapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.database.DBHandler;
import no.speiderapp.android.speiderapp.database.DBManager;
import no.speiderapp.android.speiderapp.handlers.OnlineHandler;
import no.speiderapp.android.speiderapp.objects.DBAssociation;
import no.speiderapp.android.speiderapp.objects.DBAudience;
import no.speiderapp.android.speiderapp.objects.DBAchievement;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.ODBRefs;
import no.speiderapp.android.speiderapp.reference.SPRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class SplashActivity extends AppCompatActivity {

    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Initialize context
        this.context = this;
        Utils.setEmergencyContext(this);

        // Initialize a SQLiteOpenHelper
        DBManager.initializeInstance(new DBHandler(this));

        startApplication();
    }

    private void startApplication() {
        new LoadApplicationTask(this).execute();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // OnlineHandler.getJson(ODBRefs.URL_ACHIEVEMENTS, this);
    }

    private void proceedHome() {
        // Create the new activity
        Intent nextScreen = new Intent(context, HomeActivity.class);

        // Start the new activity
        startActivity(nextScreen);

        // Close this activity, as it will not be used again!
        finish();
    }

    private class LoadApplicationTask extends AsyncTask<Void, Void, Integer> {
        private Activity context;

        private LoadApplicationTask(Activity context) {
            this.context = context;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Log.v(CRRefs.TAG, "[SplashActivity.LoadApplicationTask.onPostExecute] Achievements count: " + new DBAchievement(0).count());

            Integer nextStep;
            if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SPRefs.IMPORT_FINISHED_KEY, SPRefs.IMPORT_FINISHED_DEFAULT))
                nextStep = 0;
            else
                nextStep = 1;

            return nextStep;
        }

        @Override
        protected void onPostExecute(Integer nextStep) {
            Log.v(CRRefs.TAG, "[SplashActivity.LoadApplicationTask.onPostExecute] Selected step: " + nextStep);

            switch (nextStep) {
                case 0:
                    proceedHome();
                    break;
                case 1:
                    new OnlineHandler.LoadJsonObjectTask(new ImportAchievementsTask(context), context).execute(ODBRefs.URL_ACHIEVEMENTS, ODBRefs.URL_ASSOCIATIONS, ODBRefs.URL_AUDIENCES);
                    break;
            }
        }
    }

    private class ImportAchievementsTask extends AsyncTask<String, Integer, Boolean> {
        private Activity context;
        private ProgressDialog progressDialog;

        private ImportAchievementsTask(Activity context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle(getString(R.string.activitySplash_Dialog_Title_LoadingBadgesFromServer));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... args) {
            String str_achs = args[0];
            String str_assocs = args[1];
            String str_auds = args[2];

            int progress_now = 0;
            int progress_max = 0;

            try {
                JSONArray json_achs = new JSONObject(str_achs).getJSONArray(ODBRefs.API.DATA);
                JSONArray json_assocs = new JSONObject(str_assocs).getJSONArray(ODBRefs.API.DATA);
                JSONArray json_auds = new JSONObject(str_auds).getJSONArray(ODBRefs.API.DATA);

                int achs_length = json_achs == null ? 0 : json_achs.length();
                int assocs_length = json_assocs == null ? 0 : json_assocs.length();
                int auds_length = json_auds == null ? 0 : json_auds.length();

                progress_now = 0;
                progress_max = achs_length + assocs_length + auds_length;

                for (int i = 0; i < achs_length; i++) {
                    progress_now++;
                    JSONObject json_ach = json_achs.getJSONObject(i);
                    DBAchievement db_ach = new DBAchievement(
                            json_ach.getInt("id"),
                            json_ach.getInt("association"),
                            json_ach.getString("description"),
                            json_ach.getInt("audience"),
                            json_ach.getInt("optional") == 1, // Turn integer into boolean
                            json_ach.getInt("optionals"),
                            json_ach.getInt("link"),
                            json_ach.getInt("parent"),
                            json_ach.getInt("display"),
                            json_ach.getInt("type"),
                            json_ach.getInt("version")
                    );
                    String img_url = json_ach.getString("import_img");
                    if (!img_url.equals(""))
                        Utils.downloadIcon(img_url, String.format(getString(R.string.icon_badge), json_ach.getInt("id")), context);

                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Adding achievement: " + db_ach.verbose());
                    db_ach.save();
                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Import progress: " + i + " / " + json_ach.length());
                    publishProgress(progress_now, progress_max);
                }
                for (int i = 0; i < assocs_length; i++) {
                    progress_now++;
                    JSONObject json_assoc = json_assocs.getJSONObject(i);
                    DBAssociation db_assoc = new DBAssociation(
                            json_assoc.getInt("id"),
                            json_assoc.getString("name"),
                            json_assoc.getString("abbreviation"),
                            json_assoc.getString("country")
                    );

                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Adding association: " + db_assoc.verbose());
                    db_assoc.save();
                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Import progress: " + i + " / " + json_assoc.length());
                    publishProgress(progress_now, progress_max);
                }
                for (int i = 0; i < auds_length; i++) {
                    progress_now++;
                    JSONObject json_aud = json_auds.getJSONObject(i);
                    DBAudience db_aud = new DBAudience(
                            json_aud.getInt("id"),
                            json_aud.getString("name"),
                            json_aud.getString("age_group"),
                            json_aud.getInt("association"),
                            json_aud.getInt("order")
                    );

                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Adding audience: " + db_aud.verbose());
                    db_aud.save();
                    Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.doInBackground] Import progress: " + i + " / " + json_aud.length());
                    publishProgress(progress_now, progress_max);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            // Read all achievements from the JSONObject
            // Insert into database
            // Count new achievements
            // Return amount
            // Insert association and audience data into database

            Boolean successfullyCompleted = progress_now == progress_max && progress_max != 0;
            if (successfullyCompleted) {
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor edit = settings.edit();
                edit.putBoolean(SPRefs.IMPORT_FINISHED_KEY, true);
                edit.commit();
            }

            return successfullyCompleted;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            int max = values[1];
            int percent = (progress * 100) / max;
            Log.v(CRRefs.TAG, "[SplashActivity.ImportAchievementsTask.onProgressUpdate] Progress update: " + progress + " / " + max + " => " + percent);
            progressDialog.setMax(max);
            progressDialog.setProgress(progress);
            progressDialog.setSecondaryProgress(percent);
        }

        @Override
        protected void onPostExecute(Boolean arg) {
            progressDialog.dismiss();
            new LoadApplicationTask(context).execute();
        }
    }
}
