package no.speiderapp.android.speiderapp.utilities;

import android.database.Cursor;

public class CursorUtils {
    /**
     * getCursorString - Get a String from a DB cursor by the column name
     * @param cursor - The Cursor (DB search object) to retrieve data from
     * @param column - The Column name to search for
     * @return - The returned value
     */
    public static String getString(Cursor cursor, String column) {
        return cursor.getString(cursor.getColumnIndex(column));
    }

    /**
     * getCursorInt - Get a Integer from a DB cursor by the column name
     * @param cursor - The Cursor (DB search object) to retrieve data from
     * @param column - The Column name to search for
     * @return - The returned value
     */
    public static int getInt(Cursor cursor, String column) {
        return cursor.getInt(cursor.getColumnIndex(column));
    }
}
