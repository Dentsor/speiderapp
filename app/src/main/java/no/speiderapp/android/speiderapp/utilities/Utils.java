package no.speiderapp.android.speiderapp.utilities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.objects.DBAchievement;
import no.speiderapp.android.speiderapp.objects.DBAssociation;
import no.speiderapp.android.speiderapp.objects.DBAudience;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.SPRefs;
import no.speiderapp.android.speiderapp.utilities.External.HttpRequest;
import no.speiderapp.android.speiderapp.utilities.External.ListSelectorDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;

public class Utils {
    private static Context _context;

    public static Context getEmergencyContext() {
        return _context;
    }
    public static void setEmergencyContext(Context context) {
        _context = context;
    }

    public static String getCurrentTimestamp() {
        Long lng = System.currentTimeMillis() / 1000;
        return lng.toString();
    }

    public static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void downloadIcon(String icon_url, String local_name, Activity context) {
        Log.d(CRRefs.TAG, "[Utils.downloadIcon] Downloading icon: " + icon_url + " | " + local_name);
        try {
            URL url = new URL(icon_url);
            InputStream input = url.openStream();
            try {
                //The sdcard directory e.g. '/sdcard' can be used directly, or
                //more safely abstracted with getExternalStorageDirectory()
                //File storagePath = Environment.getExternalStorageDirectory();
                File storagePath = context.getFilesDir();
                OutputStream output = new FileOutputStream(new File(storagePath, local_name));
                try {
                    byte[] buffer = new byte[30720]; // 30 KB
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showImageFromPath(Activity activity, String filename, int viewID) {
        Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Loading image: " + filename);
        //File imgFile = new File(Environment.getExternalStorageDirectory(), filename);
        File imgFile = new File(activity.getFilesDir(), filename);
        if(imgFile.exists()){
            Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Image exists! | " + imgFile.getAbsolutePath());
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageView myImage = (ImageView) activity.findViewById(viewID);
            myImage.setImageBitmap(myBitmap);
        } else {
            Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Image doesn't exist!");
        }
    }

    public static Bitmap getBitmapFromPath(Activity activity, String filename) {
        Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Loading image: " + filename);
        //File imgFile = new File(Environment.getExternalStorageDirectory(), filename);
        File imgFile = new File(activity.getFilesDir(), filename);
        if(imgFile.exists()){
            Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Image exists! | " + imgFile.getAbsolutePath());
            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        } else {
            Log.d(CRRefs.TAG, "[Utils.showImageFromPath] Image doesn't exist!");
        }
        return null;
    }

    public static Bitmap combineBitmaps(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        return bmOverlay;
    }

    public static Bitmap getBitmapFromString(String path, Context context) {
        int resourceId = context.getResources().getIdentifier(path, "drawable", context.getPackageName());
        return BitmapFactory.decodeResource(context.getResources(), resourceId);
    }

    public static Drawable getDrawableFromString(String path, Context context) {
        int resourceId = context.getResources().getIdentifier(path, "drawable", context.getPackageName());
        return context.getResources().getDrawable(resourceId);
    }

    public static Bitmap getBitmapFromBadge(DBAchievement achievement, Context context) {
        Log.v(CRRefs.TAG, "Utils.getBitmapFromBadge: " + achievement.get_audience() + " | " + achievement.get_description());
        String path = "badge_"+achievement.get_id();
        return getBitmapFromString(path, context);
    }
    public static Drawable getDrawableFromBadge(DBAchievement achievement, Context context) {
        String path = "badge_"+achievement.get_id();
        return getDrawableFromString(path, context);
    }

    public static int pixelsFromDevicePixels(float dp, Context context) {
        return (int) (context.getResources().getDisplayMetrics().density * dp + 0.5F);
    }

    public static void setCheckedOnMatch(View view, ArrayList<String> arrayList, Context context) {
        if (view instanceof CheckBox) {
            if (arrayList.contains(((CheckBox) view).getText().toString())) {
                if (context instanceof Activity) {
                    final View fin_view = view;
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((CheckBox) fin_view).setChecked(true);
                        }
                    });
                }
            }
        }
        if (view instanceof ViewGroup) {
            int n = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < n; i++) {
                setCheckedOnMatch(((ViewGroup) view).getChildAt(i), arrayList, context);
            }
        }
    }

    public static JSONObject getJsonObjectFromUrl(String url) {
        HttpRequest request = HttpRequest.get(url);
        JSONObject json = null;
        try {
            json = new JSONObject(request.body());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONArray getJsonArrayFromUrl(String url) {
        HttpRequest request = HttpRequest.get(url);
        JSONArray json = null;
        try {
            json = new JSONArray(request.body());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String getJsonString(JSONObject json, String name, String defaultValue) {
        String ret;
        try {
            ret = json.getString(name);
        } catch (Exception e) {
            ret = defaultValue;
        }
        return ret;
    }

    public static int getJsonInt(JSONObject json, String name, int defaultValue) {
        int ret;
        try {
            ret = json.getInt(name);
        } catch (Exception e) {
            ret = defaultValue;
        }
        return ret;
    }

    /**
     * Helper function that parses a given table into a string
     * and returns it for easy printing. The string consists of
     * the table name and then each row is iterated through with
     * column_name: value pairs printed out.
     *
     * @param db the database to get the table from
     * @param tableName the the name of the table to parse
     * @return the table tableName as a string
     */
    public static String getTableAsString(SQLiteDatabase db, String tableName) {
        Log.d(CRRefs.TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);

        allRows.moveToFirst();
        while (!allRows.isAfterLast()) {
            boolean isFirst = true;
            for (String name : allRows.getColumnNames()) {
                tableString += String.format(
                        "%s%s = %s",
                        (isFirst ? "" : "  |  "),
                        name,
                        allRows.getString(allRows.getColumnIndex(name))
                );
                isFirst = false;
            }
            tableString += "\n";
            allRows.moveToNext();
        }
        Log.v(CRRefs.TAG, "[Utils.getTableAsString]: allRows.count: " + allRows.getCount());
        allRows.close();

        Log.d(CRRefs.TAG, tableString);

        return tableString;
    }

    public static BlockingOnUIRunnable runOnUIAndWait(Activity context, final Runnable runnable) {
        return new BlockingOnUIRunnable(context, new BlockingOnUIRunnableListener() {
            public void onRunOnUIThread() {
                runnable.run();
            }
        });
    }

    /**
     * @param view         View to animate
     * @param toVisibility Visibility at the end of animation
     * @param toAlpha      Alpha at the end of animation
     * @param duration     Animation duration in ms
     */
    public static void animateView(final View view, final int toVisibility, float toAlpha, int duration) {
        boolean show = toVisibility == View.VISIBLE;
        if (show) {
            view.setAlpha(0);
        }
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setDuration(duration)
                .alpha(show ? toAlpha : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(toVisibility);
                    }
                });
    }

    public static class CheckSettingsTask extends AsyncTask<Void, ArrayList<String>, Boolean> {
        private Activity context;
        private ProgressDialog progressDialog;
        private Boolean recreateActivityOnFinish;
        private Integer actions = 0;

        public CheckSettingsTask(Activity context) {
            this(context, false);
        }
        public CheckSettingsTask(Activity context, Boolean recreateActivityOnFinish) {
            this.context = context;
            this.recreateActivityOnFinish = recreateActivityOnFinish;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getString(R.string._Dialog_Loading_Title));
            progressDialog.setMessage(context.getString(R.string._Dialog_Loading_Message));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

            String association = settings.getString(SPRefs.PROFILE_ASSOCIATION_KEY, null);
            if (association == null) {
                ArrayList<String>[] assocs = new DBAssociation(0).getEntries();
                ArrayList<String> assoc_keys = assocs[0];
                ArrayList<String> assoc_vals = assocs[1];
                ArrayList<String> assoc_args = new ArrayList<>();
                assoc_args.add(context.getString(R.string.activityHome_Dialog_Association_Title));
                assoc_args.add(SPRefs.PROFILE_ASSOCIATION_KEY);
                actions++;
                publishProgress(assoc_args, assoc_keys, assoc_vals);
                return false;
            }

            String audience = settings.getString(SPRefs.PROFILE_AUDIENCE_KEY, null);
            if (audience == null) {
                ArrayList<String>[] auds = new DBAudience(0).getEntries(Integer.parseInt(settings.getString(SPRefs.PROFILE_ASSOCIATION_KEY, null)));
                ArrayList<String> aud_keys = auds[0];
                ArrayList<String> aud_vals = auds[1];
                ArrayList<String> aud_args = new ArrayList<>();
                aud_args.add(context.getString(R.string.activityHome_Dialog_Audience_Title));
                aud_args.add(SPRefs.PROFILE_AUDIENCE_KEY);
                actions++;
                publishProgress(aud_args, aud_keys, aud_vals);
                return false;
            }

            return true;
        }

        @Override
        protected void onProgressUpdate(ArrayList<String>... values) {
            ArrayList<String> list_args = values[0];
            String str_title = list_args.get(0);
            final String str_key = list_args.get(1);
            ArrayList<String> list_keys = values[1];
            ArrayList<String> list_vals = values[2];

            progressDialog.dismiss();

            ListSelectorDialog dlg = new ListSelectorDialog(context, str_title);
            dlg.setCancelable(false);
            dlg.show(list_vals, list_keys, new ListSelectorDialog.listSelectorInterface() {
                @Override
                public void selectedItem(String key, String item) {
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(str_key, key);
                    editor.apply();
                    new CheckSettingsTask(context, recreateActivityOnFinish).execute();
                }

                @Override
                public void selectorCanceled() {}
            });
        }

        @Override
        protected void onPostExecute(Boolean complete) {
            if (complete)
                progressDialog.dismiss();

            if (recreateActivityOnFinish && actions == 0) {
                context.recreate();
            }
        }
    }
}
