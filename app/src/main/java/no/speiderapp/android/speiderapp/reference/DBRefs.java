package no.speiderapp.android.speiderapp.reference;

/*
 * Database References
 */
public class DBRefs {
    public static final int DATABASE_VERSION = 50;
    public static final String DATABASE_NAME = "speiderapp.db";

    public static final class Tables {
        public static final String ACHIEVEMENTS = "achievements";
        public static final String PROGRESSION = "progression";
        public static final String PROFILES = "profiles";
        public static final String ASSOCIATIONS = "associations";
        public static final String AUDIENCES = "audiences";
    }

    public static final class Columns {
        public static final String ID = "_id";
    }

    public static final class ColumnsAchievements {
        public static final String ID = Columns.ID;
        public static final String ASSOCIATION = "association";
        public static final String DESCRIPTION = "description";
        public static final String AUDIENCE = "audience";
        public static final String OPTIONAL = "optional";
        public static final String OPTIONALS = "optionals";
        public static final String LINK = "link";
        public static final String PARENT = "parent";
        public static final String DISPLAY = "display";
        public static final String TYPE = "type";
        public static final String VERSION = "version";

    }

    public static final class ColumnsProgression {
        public static final String ID = Columns.ID;
        public static final String ACHID = "achid";
        public static final String PROFILE = "profile";
        public static final String PROGRESS = "progress";
        public static final String LAST_CHANGED = "last_changed";
        public static final String LAST_SYNCED = "last_synced";
    }

    public static final class ColumnsProfiles {
        public static final String ID = Columns.ID;
        public static final String NAME = "name";
    }

    public static final class ColumnsAssociations {
        public static final String ID = Columns.ID;
        public static final String NAME = "name";
        public static final String ABBREVIATION = "abbreviation";
        public static final String COUNTRY = "country";
    }

    public static final class ColumnsAudiences {
        public static final String ID = Columns.ID;
        public static final String NAME = "name";
        public static final String AGE_GROUP = "age_group";
        public static final String ASSOCIATION = "association";
        public static final String DISPLAY_ORDER = "display_order";
    }
}
