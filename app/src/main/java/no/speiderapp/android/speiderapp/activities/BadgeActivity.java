package no.speiderapp.android.speiderapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.objects.DBAchievement;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.EXRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class BadgeActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    int containerID = R.id.activityBadge_LinearLayout_RequirementsList;
    int badgeID;
    int loadingTasks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badge);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        badgeID = getIntent().getIntExtra(EXRefs.ACHIEVEMENT_ID, 0);
        new LoadAchievementsTask(this, badgeID, containerID).execute();

        // Load image
        Utils.showImageFromPath(this, String.format(getString(R.string.icon_badge), badgeID), R.id.activityBadge_ImageView_BadgeIcon);

        // TODO: Move from UI Thread
        DBAchievement ach = new DBAchievement(badgeID).get();
        setTitle(ach.get_description());
    }

    private class LoadAchievementsTask extends AsyncTask<Void, String, Void> {
        private Context context;
        private int parent_id;
        private int container_id;

        private int optionalsRequired;
        private boolean header_mandatory = false;
        private boolean header_optional = false;
        private boolean header_completed = false;

        public LoadAchievementsTask(Context context, int parent_id, int container_id) {
            this.context = context;
            this.parent_id = parent_id;
            this.container_id = container_id;
        }
        public LoadAchievementsTask(Context context, int parent_id, String container_tag) {
            this.context = context;
            this.parent_id = parent_id;
            this.container_id = findViewById(containerID).findViewWithTag(container_tag).getId();
        }

        @Override
        protected void onPreExecute() {
            if (loadingTasks == 0) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setTitle(getString(R.string.activityBadge_Dialog_Title_LoadingBadge));
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
            loadingTasks++;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            DBAchievement badge = new DBAchievement(badgeID).get();
            DBAchievement parent = new DBAchievement(parent_id).get();
            optionalsRequired = parent.get_optionals();
            ArrayList<Integer> children = parent.getChildren();

            ArrayList<DBAchievement> mandatory_todo = new ArrayList<>();
            ArrayList<DBAchievement> mandatory_comp = new ArrayList<>();
            ArrayList<DBAchievement> optional_todo = new ArrayList<>();
            ArrayList<DBAchievement> optional_comp = new ArrayList<>();

            // TODO: Sort by Display Order

            for (int child : children) {
                DBAchievement _ach = new DBAchievement(child).get();
                if (!_ach.is_optional()) {
                    if (!_ach.is_completed(context))
                        mandatory_todo.add(_ach);
                    else
                        mandatory_comp.add(_ach);
                } else {
                    if (!_ach.is_completed(context))
                        optional_todo.add(_ach);
                    else
                        optional_comp.add(_ach);
                }
            }

            ArrayList<DBAchievement>[] displayOrder = new ArrayList[]{mandatory_todo, optional_todo, optional_comp, mandatory_comp};
            for (ArrayList<DBAchievement> _list : displayOrder) {
                for (DBAchievement _ach : _list) {
                    publishProgress(Integer.toString(_ach.get_id()), _ach.get_description(), Integer.toString(_ach.getChildren().size()), Integer.toString(_ach.is_completed(context) ? 1 : 0), Integer.toString(_ach.is_optional() ? 1 : 0), Integer.toString(badge.is_completed(context) ? 1 : 0));
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            int ach_id = Integer.parseInt(values[0]);
            String ach_desc = values[1];
            int ach_childCount = Integer.parseInt(values[2]);
            boolean ach_isCompleted = Integer.parseInt(values[3]) == 1;
            boolean ach_isOptional = Integer.parseInt(values[4]) == 1;
            boolean badge_completed = Integer.parseInt(values[5]) == 1;

            String cont_tag = "achievementContainer_" + ach_id;
            String kids_tag = "achievementChildren_" + ach_id;
            String ach_tag = "achievementId_" + ach_id;

            Log.v(CRRefs.TAG, "[BadgeActivity.LoadAchievementsTask.onProgressUpdate] Progress update: id | desc | tag | opt | comp : " + ach_id + " | " + ach_desc + " | " + ach_tag + " | " + ach_isOptional + " | " + ach_isCompleted);

            if (badge_completed) {
                findViewById(R.id.activityBadge_LinearLayout_BadgeCompleted).setVisibility(View.VISIBLE);
            }

            TextView vHeader = null;
            if (!ach_isOptional && !ach_isCompleted && !header_mandatory) {
                vHeader = new TextView(context);
                vHeader.setText(getString(R.string.activityBadge_TextView_Header_MandatoryRequirements));
                header_mandatory = true;
            }
            if (ach_isOptional && !header_optional) {
                vHeader = new TextView(context);
                vHeader.setText(String.format(getString(R.string.activityBadge_TextView_Header_OptionalRequirements_full), Integer.toString(optionalsRequired)));
                header_optional = true;
            }
            if (!ach_isOptional && ach_isCompleted && !header_completed) {
                vHeader = new TextView(context);
                vHeader.setText(getString(R.string.activityBadge_TextView_Header_MandatoryRequirementsFinished));
                header_completed = true;
            }
            if (vHeader != null) {
                LinearLayout.LayoutParams lpHeader = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lpHeader.setMargins(0, Utils.pixelsFromDevicePixels(20F, context), 0, 0);
                vHeader.setLayoutParams(lpHeader);
                vHeader.setGravity(Gravity.CENTER);
                vHeader.setTextSize(18F);
                ((LinearLayout) findViewById(container_id)).addView(vHeader);
            }

            LinearLayout.LayoutParams lpRow = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            View vItem;
            if (ach_childCount >= 1) {
                vItem = new Button(context);
                ((Button)vItem).setText(ach_desc);
            } else {
                vItem = new CheckBox(context);
                ((CheckBox) vItem).setTextSize(16F);
                ((CheckBox) vItem).setText(ach_desc);
                ((CheckBox) vItem).setChecked(ach_isCompleted);
                if (ach_isCompleted)
                    ((CheckBox) vItem).setPaintFlags(((CheckBox) vItem).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                ((CheckBox) vItem).setOnCheckedChangeListener(new onAchievementClickedListener(ach_id));
            }

            LinearLayout vgReq = new LinearLayout(context);
            vgReq.setOrientation(LinearLayout.HORIZONTAL);
            vgReq.setTag(ach_tag);
            vgReq.setLayoutParams(lpRow);
            vgReq.addView(vItem);

            LinearLayout vgKids = new LinearLayout(context);
            vgKids.setOrientation(LinearLayout.HORIZONTAL);
            vgKids.setTag(kids_tag);
            vgKids.setLayoutParams(lpRow);
            vgKids.setVisibility(View.GONE);

            LinearLayout vgAchievement = new LinearLayout(context);
            vgAchievement.setOrientation(LinearLayout.HORIZONTAL);
            vgAchievement.setTag(cont_tag);
            vgAchievement.setLayoutParams(lpRow);
            vgAchievement.addView(vgReq);
            vgAchievement.addView(vgKids);

            ((LinearLayout) findViewById(container_id)).addView(vgAchievement);

            new LoadAchievementsTask(context, ach_id, kids_tag).execute();
        }

        @Override
        protected void onPostExecute(Void arg) {
            loadingTasks--;

            if (loadingTasks == 0) {
                progressDialog.dismiss();
            }
        }
    }

    private class onAchievementClickedListener implements CheckBox.OnCheckedChangeListener {
        private int ach_id;

        public onAchievementClickedListener(int achievement_id) {
            this.ach_id = achievement_id;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.v(CRRefs.TAG, "[BadgeActivity.onAchievementClickedListener.onCheckedChanged] Clicked: " + ach_id + " | " + (isChecked ? "true" : "false"));
            new CheckAchievementTask(ach_id, isChecked).execute();
        }
    }

    private class CheckAchievementTask extends AsyncTask<Void, Void, Integer> {
        private int ach_id;
        private boolean isChecked;

        public CheckAchievementTask(int achievement_id, boolean isChecked) {
            this.ach_id = achievement_id;
            this.isChecked = isChecked;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            DBAchievement ach = new DBAchievement(ach_id).get();
            ach.set_complete(getApplicationContext(), isChecked);
            // new DBAchievement(ach.get_parent()).get().calculateProgress(getApplicationContext());
            return new DBAchievement(badgeID).get().calculateProgress(getApplicationContext());
        }

        @Override
        protected void onPostExecute(Integer integer) {
            findViewById(R.id.activityBadge_LinearLayout_BadgeCompleted).setVisibility(integer == 100 ? View.VISIBLE : View.GONE);
        }
    }
}
