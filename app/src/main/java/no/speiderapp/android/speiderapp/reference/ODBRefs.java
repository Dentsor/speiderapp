package no.speiderapp.android.speiderapp.reference;

/*
 * Online Database References
 */
public class ODBRefs {
    public static final String URL_ACHIEVEMENTS = "https://filibuster.hw.speiderapp.no/api/v0.2/achievements";
    public static final String URL_ASSOCIATIONS = "https://filibuster.hw.speiderapp.no/api/v0.2/associations";
    public static final String URL_AUDIENCES = "https://filibuster.hw.speiderapp.no/api/v0.2/audiences";
    // public static final String URL_DEVICES = "https://api.speiderapp.no/api/v0.2/devices";

    public static final class API {
        public static final String DATA = "data";
    }
    public static final class ACHIEVEMENTS {
        public static final String ACHKEY = "achkey";
        public static final String ASSOCIATION = "association";
        public static final String DESCRIPTION = "description";
        public static final String LEVEL = "level";
        public static final String TIER = "tier";
        public static final String OPTIONAL = "optional";
        public static final String OPTIONALS = "optionals";
        public static final String LINK = "link";
        public static final String PARENT = "parent";
        public static final String DISPLAY = "display";
        public static final String MODE = "mode";
        public static final String BADGE_KEY = "badge_key";
        public static final String VERSION = "version";
    }
}
