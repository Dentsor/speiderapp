package no.speiderapp.android.speiderapp.handlers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONObject;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class OnlineHandler {
    public static class LoadJsonObjectTask extends AsyncTask<String, Void, String[]> {
        AsyncTask<String, ?, ?> callback;
        Context context;
        private ProgressDialog progressDialog;

        public LoadJsonObjectTask(AsyncTask<String, ?, ?> callback, Context context) {
            this.callback = callback;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context, "Loading", "Retrieving data from server...");
        }

        @Override
        protected String[] doInBackground(String... strings) {
            String[] ret = new String[strings.length];
            for (int i = 0; i < strings.length; i++) {
                Log.v(CRRefs.TAG, "[OnlineHandler.LoadJsonObjectTask.doOnBackground] Loading JsonObject from URL: " + strings[i]);
                JSONObject jsonObject = Utils.getJsonObjectFromUrl(strings[i]);
                ret[i] = jsonObject != null ? jsonObject.toString() : "";
            }
            return ret;
        }

        @Override
        protected void onPostExecute(String[] jsonStrings) {
            progressDialog.dismiss();
            Log.v(CRRefs.TAG, "[OnlineHandler.LoadJsonObjectTask.doOnBackground] Finished loading JsonObject!");
            callback.execute(jsonStrings);
        }
    }
}
