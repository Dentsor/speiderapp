package no.speiderapp.android.speiderapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.reference.ERRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class HomeActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Assign toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Init
        context = this;
        setTitle(getString(R.string.app_name));

        // Assign onclick listeners
        findViewById(R.id.activityHome_Button_Badges).setOnClickListener(new onBadgesClickedListener());
        findViewById(R.id.activityHome_Button_Settings).setOnClickListener(new onSettingsClickedListener());

        new Utils.CheckSettingsTask(this).execute();
    }

    private class onBadgesClickedListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Create the new activity
            Intent nextScreen = new Intent(getApplicationContext(), BadgesActivity.class);

            // Start the new activity
            startActivity(nextScreen);
        }
    }

    private class onSettingsClickedListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Create the new activity
            Intent nextScreen = new Intent(getApplicationContext(), PreferenceActivity.class);

            // Start the new activity
            startActivity(nextScreen);
        }

    }
}
