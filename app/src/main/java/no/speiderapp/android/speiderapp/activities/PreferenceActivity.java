package no.speiderapp.android.speiderapp.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.database.DBHandler;
import no.speiderapp.android.speiderapp.database.DBManager;
import no.speiderapp.android.speiderapp.objects.DBAssociation;
import no.speiderapp.android.speiderapp.objects.DBAudience;
import no.speiderapp.android.speiderapp.reference.SPRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class PreferenceActivity extends AppCompatPreferenceActivity {

    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(this);

        PreferenceCategory category = new PreferenceCategory(this);
        category.setTitle(getString(R.string.activityPreference_PreferenceCategory_Profile_Title));
        screen.addPreference(category);


        ListPreference lp_association = new ListPreference(this);
        ArrayList<String>[] assocs = new DBAssociation(0).getEntries();
        ArrayList<String> assoc_keys = assocs[0];
        ArrayList<String> assoc_vals = assocs[1];
        lp_association.setTitle(getString(R.string.activityPreference_ListPreference_Association_Title));
        lp_association.setSummary("%s");
        lp_association.setEntries(assoc_vals.toArray(new String[0]));
        lp_association.setEntryValues(assoc_keys.toArray(new String[0]));
        lp_association.setKey(SPRefs.PROFILE_ASSOCIATION_KEY);
        lp_association.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor edit = settings.edit();
                edit.remove(SPRefs.PROFILE_AUDIENCE_KEY);
                edit.apply();
                new Utils.CheckSettingsTask(context, true).execute();
                return true;
            }
        });
        category.addPreference(lp_association);

        ListPreference lp_audience = new ListPreference(this);
        ArrayList<String>[] auds = new DBAudience(0).getEntries(PreferenceManager.getDefaultSharedPreferences(context).getString(SPRefs.PROFILE_ASSOCIATION_KEY,SPRefs.PROFILE_ASSOCIATION_DEFAULT));
        ArrayList<String> aud_keys = auds[0];
        ArrayList<String> aud_vals = auds[1];
        lp_audience.setTitle(getString(R.string.activityPreference_ListPreference_Audience_Title));
        lp_audience.setSummary("%s");
        lp_audience.setEntries(aud_vals.toArray(new String[0]));
        lp_audience.setEntryValues(aud_keys.toArray(new String[0]));
        lp_audience.setKey(SPRefs.PROFILE_AUDIENCE_KEY);
        lp_audience.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                return true;
            }
        });
        category.addPreference(lp_audience);

        Preference bp_resetDatabase = new Preference(this);
        bp_resetDatabase.setTitle(getString(R.string.activityPreference_Preference_ResetDatabase_Title));
        bp_resetDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setTitle(getString(R.string.activityPreference_Dialog_ResetDatabase_Title));
                adb.setMessage(getString(R.string.activityPreference_Dialog_ResetDatabase_Message));
                adb.setPositiveButton(getString(R.string._Generic_Delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        DBManager dbManager = DBManager.getInstance(new DBHandler());
                        SQLiteDatabase db = dbManager.openDatabase();

                        DBHandler dbHandler = new DBHandler(context);
                        dbHandler.purgeDatabase(db);
                        dbHandler.buildDatabase(db);

                        dbManager.closeDatabase();

                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor edit = settings.edit();
                        edit.remove(SPRefs.IMPORT_FINISHED_KEY);
                        edit.commit();

                        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                });
                adb.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                adb.show();
                return true;
            }
        });
        category.addPreference(bp_resetDatabase);

        Preference bp_resetPreferences = new Preference(this);
        bp_resetPreferences.setTitle(getString(R.string.activityPreference_Preference_ResetPreferences_Title));
        bp_resetPreferences.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setTitle(getString(R.string.activityPreference_Dialog_ResetPreferences_Title));
                adb.setMessage(getString(R.string.activityPreference_Dialog_ResetPreferences_Message));
                adb.setPositiveButton(getString(R.string._Generic_Delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor e = settings.edit();
                        e.clear();
                        e.apply();

                        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                });
                adb.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                adb.show();
                return true;
            }
        });
        category.addPreference(bp_resetPreferences);

        setPreferenceScreen(screen);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
