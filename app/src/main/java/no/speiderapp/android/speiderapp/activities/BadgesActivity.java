package no.speiderapp.android.speiderapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import no.speiderapp.android.speiderapp.R;
import no.speiderapp.android.speiderapp.objects.DBAchievement;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.EXRefs;
import no.speiderapp.android.speiderapp.reference.SPRefs;
import no.speiderapp.android.speiderapp.utilities.Utils;

public class BadgesActivity extends AppCompatActivity {

    private Activity context;
    private int containerID = R.id.activityBadges_LinearLayout_BadgesList;
    private boolean header_completed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badges);

        // init
        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle(getString(R.string.toolbar_badges));
    }

    @Override
    protected void onResume() {
        super.onResume();

        //new LoadHeaderTask(context).execute();
        new LoadBadgesTask(context).execute();
    }

    private class LoadBadgesTask extends AsyncTask<Void, String, Integer> {
        private Activity context;
        private ProgressDialog progressDialog;

        public LoadBadgesTask(Activity context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setTitle(getString(R.string.activityBadges_Dialog_Title_LoadingBadges));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMax(1);
            progressDialog.setProgress(0);
            progressDialog.show();

            ((ViewGroup) findViewById(containerID)).removeAllViews();
        }

        @Override
        protected Integer doInBackground(Void... args) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            int association = Integer.parseInt(settings.getString(SPRefs.PROFILE_ASSOCIATION_KEY, SPRefs.PROFILE_ASSOCIATION_DEFAULT));
            int audience = Integer.parseInt(settings.getString(SPRefs.PROFILE_AUDIENCE_KEY, SPRefs.PROFILE_AUDIENCE_DEFAULT));

            ArrayList<Integer> list = new DBAchievement(association, audience).getAchievements();

            int counter = 0;
            int length = list.size();
            ArrayList<DBAchievement> badges_todo = new ArrayList<>();
            ArrayList<DBAchievement> badges_comp = new ArrayList<>();
            for (int i : list) {
                DBAchievement achievement = new DBAchievement(i).get();
                counter++;

                if (achievement.is_completed(context))
                    badges_comp.add(achievement);
                else
                    badges_todo.add(achievement);
            }

            Collections.sort(badges_todo, new Comparator<DBAchievement>() {
                @Override
                public int compare(DBAchievement o1, DBAchievement o2) {
                    return o2.get_progress(context) - o1.get_progress(context);
                }
            });

            ArrayList<DBAchievement>[] displayOrder = new ArrayList[]{badges_todo, badges_comp};
            for (ArrayList<DBAchievement> _list : displayOrder) {
                for (DBAchievement achievement : _list) {
                    publishProgress(Integer.toString(counter), Integer.toString(length), Integer.toString(achievement.get_id()), achievement.get_description(), Integer.toString(achievement.get_progress(context)));
                }
            }

            return audience;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            int log_progress = Integer.parseInt(values[0]);
            int log_max = Integer.parseInt(values[1]);
            int log_percent = (log_progress * 100) / log_max;
            int badge_id = Integer.parseInt(values[2]);
            String badge_name = values[3];
            int badge_progress = Integer.parseInt(values[4]);
            boolean badge_isCompleted = badge_progress == 100;

            int dimImage = 80;

            Log.v(CRRefs.TAG, "[BadgesActivity.LoadBadgesTask.onProgressUpdate] Progress update: " + log_progress + " / " + log_max + " => " + log_percent);
            Log.v(CRRefs.TAG, "[BadgesActivity.LoadBadgesTask.onProgressUpdate] Badge: [" + badge_id + "] '" + badge_name + "' Progress: " + badge_progress);
            progressDialog.setMax(log_max);
            progressDialog.setProgress(log_progress);
            progressDialog.setSecondaryProgress(log_percent);

            TextView vHeader = null;
            if (badge_isCompleted && !header_completed) {
                vHeader = new TextView(context);
                vHeader.setText(getString(R.string.activityBadges_TextView_Header_BadgesCompleted));
                header_completed = true;

                LinearLayout.LayoutParams lpHeader = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lpHeader.setMargins(0, Utils.pixelsFromDevicePixels(20F, context), 0, 0);
                vHeader.setLayoutParams(lpHeader);
                vHeader.setGravity(Gravity.CENTER);
                vHeader.setTextSize(18F);
                ((LinearLayout) findViewById(containerID)).addView(vHeader);
            }

            LinearLayout vgRow = new LinearLayout(context);
            vgRow.setOrientation(LinearLayout.HORIZONTAL);

            ImageView vImage = new ImageView(context);
            LinearLayout.LayoutParams imgDetails = new LinearLayout.LayoutParams(
                    dimImage,
                    dimImage,
                    0F
            );
            imgDetails.gravity = Gravity.CENTER_VERTICAL;
            Bitmap bm = Utils.getBitmapFromPath(context, String.format(getString(R.string.icon_badge), badge_id));
            if (bm != null) {
                vImage.setImageBitmap(bm);
            } else
                vImage.setImageDrawable(Utils.getDrawableFromString("ic_missing", context));
            vgRow.addView(vImage, imgDetails);

            Button vButton = new Button(context);
            vButton.setText(badge_name);
            vButton.setOnClickListener(new onBadgeClickedListener(badge_id));
            LinearLayout.LayoutParams btnDetails = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1F
            );
            vgRow.addView(vButton, btnDetails);

            LinearLayout.LayoutParams prgDetails = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0F
            );
            prgDetails.setMargins(0, 0, 10, 0);
            prgDetails.width = dimImage;
            prgDetails.height = dimImage;
            prgDetails.gravity = Gravity.CENTER_VERTICAL;
            if (badge_progress < 100) {
                TextView vProgress = new TextView(context);
                vProgress.setText(String.format(getString(R.string._Generic_Percent), badge_progress));
                vgRow.addView(vProgress, prgDetails);
            } else {
                ImageView vTick = new ImageView(context);
                vTick.setImageDrawable(Utils.getDrawableFromString("ic_tick", context));
                vgRow.addView(vTick, prgDetails);
            }

            ((ViewGroup) findViewById(containerID)).addView(vgRow);
        }

        @Override
        protected void onPostExecute(Integer arg) {
            progressDialog.dismiss();
        }
    }

    private class onBadgeClickedListener implements View.OnClickListener {

        private int achievementID;

        public onBadgeClickedListener(int achievementID) {
            this.achievementID = achievementID;
        }

        @Override
        public void onClick(View v) {
            Log.v(CRRefs.TAG, "BadgesActivity [onBadgeClickedListener.onBadgeClickedListener]: Badge clicked: " + achievementID);
            Intent nextScreen = new Intent(getApplicationContext(), BadgeActivity.class);
            nextScreen.putExtra(EXRefs.ACHIEVEMENT_ID, achievementID);
            startActivity(nextScreen);
        }
    }
}
