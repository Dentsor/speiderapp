package no.speiderapp.android.speiderapp.utilities;

public class ClassUtils {
    public interface CharSequenceArrayTwoDim {
        CharSequence[][] get();
    }
}
