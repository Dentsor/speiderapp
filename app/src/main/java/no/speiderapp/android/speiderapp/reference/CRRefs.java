package no.speiderapp.android.speiderapp.reference;

/*
 * Core References
 */
public class CRRefs {
    public static final String TAG = "SPEIDERAPP";
    public static final int PROFILE_GLOBAL_SETTING = 0;
    public static final int PERMISSIONS_REQUEST_CODE = 29537; // sa to binary to decimal
}
