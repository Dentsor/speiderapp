package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public class DBAudience extends DBObject<DBAudience> {
    private DBAudience() {
        super(new ContentValues());
    }

    protected DBAudience(ContentValues values) {
        super(values);
    }
    public DBAudience(int id) {
        this();
        set_id(id);
    }
    public DBAudience(int id, String name, String ageGroup, int association, int display_order) {
        this();
        set_id(id);
        set_name(name);
        set_ageGroup(ageGroup);
        set_association(association);
        set_displayOrder(display_order);
    }

    @Override
    protected String getTable() {
        return DBRefs.Tables.AUDIENCES;
    }

    @Override
    public String getWhereSQL() {
        return DBRefs.ColumnsAudiences.ID + " = '" + this.get_id() + "'";
    }

    @Override
    protected void setValuesFromCursor(Cursor cursor) {
        set_id(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.ID));
        set_name(CursorUtils.getString(cursor, DBRefs.ColumnsAudiences.NAME));
        set_ageGroup(CursorUtils.getString(cursor, DBRefs.ColumnsAudiences.AGE_GROUP));
        set_association(CursorUtils.getInt(cursor, DBRefs.ColumnsAudiences.ASSOCIATION));
        set_displayOrder(CursorUtils.getInt(cursor, DBRefs.ColumnsAudiences.DISPLAY_ORDER));
    }


    // Getters
    public String get_name() {
        return getAsString(DBRefs.ColumnsAudiences.NAME);
    }
    public String get_ageGroup() {
        return getAsString(DBRefs.ColumnsAudiences.AGE_GROUP);
    }
    public int get_association() {
        return getAsInteger(DBRefs.ColumnsAudiences.ASSOCIATION);
    }
    public int get_displayOrder() {
        return getAsInteger(DBRefs.ColumnsAudiences.DISPLAY_ORDER);
    }

    public ArrayList<String>[] getEntries() {
        ArrayList<Integer> aud_ids = new DBAudience(0).getAll();
        ArrayList<DBAudience> auds = new ArrayList<>();
        for (Integer id : aud_ids) {
            auds.add(new DBAudience(id).get());
        }
        Collections.sort(auds, new Comparator<DBAudience>() {
            @Override
            public int compare(DBAudience lhs, DBAudience rhs) {
                return lhs.get_displayOrder() - rhs.get_displayOrder();
            }
        });
        ArrayList<String> aud_keys = new ArrayList<>();
        ArrayList<String> aud_vals = new ArrayList<>();
        for (DBAudience aud : auds) {
            aud_keys.add(Integer.toString(aud.get_id()));
            aud_vals.add(aud.get_name() + " (" + aud.get_ageGroup() + ")");
        }
        return new ArrayList[] {aud_keys, aud_vals};
    }
    public ArrayList<String>[] getEntries(String associationID) {
        return getEntries(Integer.parseInt(associationID));
    }
    public ArrayList<String>[] getEntries(int association) {
        ArrayList<Integer> aud_ids = new DBAudience(0).getWhere(DBRefs.ColumnsAudiences.ASSOCIATION + " =  '"+association+"'");
        ArrayList<DBAudience> auds = new ArrayList<>();
        for (Integer id : aud_ids) {
            auds.add(new DBAudience(id).get());
        }
        Collections.sort(auds, new Comparator<DBAudience>() {
            @Override
            public int compare(DBAudience lhs, DBAudience rhs) {
                return lhs.get_displayOrder() - rhs.get_displayOrder();
            }
        });
        ArrayList<String> aud_keys = new ArrayList<>();
        ArrayList<String> aud_vals = new ArrayList<>();
        for (DBAudience aud : auds) {
            aud_keys.add(Integer.toString(aud.get_id()));
            aud_vals.add(aud.get_name() + " (" + aud.get_ageGroup() + ")");
        }
        return new ArrayList[] {aud_keys, aud_vals};
    }


    // Setters
    public void set_name(String value) {
        put(DBRefs.ColumnsAudiences.NAME, value);
    }
    public void set_ageGroup(String value) {
        put(DBRefs.ColumnsAudiences.AGE_GROUP, value);
    }
    public void set_association(int value) {
        put(DBRefs.ColumnsAudiences.ASSOCIATION, value);
    }
    public void set_displayOrder(int value) {
        put(DBRefs.ColumnsAudiences.DISPLAY_ORDER, value);
    }
}
