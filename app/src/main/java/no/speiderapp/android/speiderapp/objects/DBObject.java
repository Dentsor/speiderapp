package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import no.speiderapp.android.speiderapp.database.DBHandler;
import no.speiderapp.android.speiderapp.database.DBManager;
import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public abstract class DBObject<T extends DBObject<T>> {
    private DBManager dbManager;

    private ContentValues values;

    // #######################
    // ###   CONSTRUCTOR   ###
    // #######################
    protected DBObject(ContentValues values) {
        this.values = values;
        dbManager = DBManager.getInstance(new DBHandler());
    }

    // ##################
    // ###   SETTER   ###
    // ##################
    protected void put(String key, String value) {
        values.put(key, value);
    }
    protected void put(String key, int value) {
        values.put(key, value);
    }
    protected void put(String key, boolean value) {
        values.put(key, value);
    }

    public void set_id(int value) {
        put(DBRefs.ColumnsProfiles.ID, value);
    }

    // ##################
    // ###   GETTER   ###
    // ##################
    protected Object get(String key) {
        return values.get(key);
    }
    protected String getAsString(String key) {
        String ret = values.getAsString(key);
        return ret == null ? "" : ret;
    }
    protected int getAsInteger(String key) {
        if (values.containsKey(key))
            return values.getAsInteger(key);
        else
            return 0;
    }
    protected boolean getAsBoolean(String key) {
        return values.getAsBoolean(key);
    }

    public int get_id() {
        return getAsInteger(DBRefs.ColumnsProfiles.ID);
    }
    protected ContentValues getValues() {
        return values;
    }
    protected boolean hasProperty(String key) {
        return values.containsKey(key);
    }
    private String getIDWhereSQL() {
        return DBRefs.Columns.ID + " = '" + this.get_id() + "'";
    }

    // ####################
    // ###   ABSTRACT   ###
    // ####################
    protected abstract String getTable();
    public abstract String getWhereSQL(); // return DBRefs.Columns.ID + " = '" + this.get_id() + "'";
    protected abstract void setValuesFromCursor(Cursor cursor); // values.put(COLUMN_ID, cursor.getInt(cursor.getColumnIndex(COLUMN_ID))); // Use CursorUtils

    // ##################
    // ###  DATABASE  ###
    // ##################
    public long save() {
        SQLiteDatabase db = dbManager.openDatabase();
        Log.v(CRRefs.TAG, "[DBObject.save]: Saving entry " + verbose());
        long ret;
        // If object exists, then update, else insert
        if (exists()) {
            if (this.hasProperty(DBRefs.Columns.ID)) {
                ret = (long) db.update(getTable(), getValues(), getIDWhereSQL(), null);
            } else {
                ret = (long) db.update(getTable(), getValues(), getWhereSQL(), null);
            }
            Log.v(CRRefs.TAG, "[DBObject.save]: Updated: " + getTable() + " | " + values.toString() + " | " + verbose());
        } else {
            ContentValues values = getValues();
            ret = db.insert(getTable(), null, values);
            this.set_id((int) ret);
            Log.v(CRRefs.TAG, "[DBObject.save]: Added: " + getTable() + " | " + values.toString() + " | " + verbose() + " TEMP: " + ret);
        }
        this.get();

        dbManager.closeDatabase();
        return ret;
    }
    public void delete() {
        SQLiteDatabase db = dbManager.openDatabase();
        Log.v(CRRefs.TAG, "[DBObject.delete]: Deleting entry " + verbose());
        db.execSQL("DELETE FROM " + getTable() + " WHERE " + getWhereSQL() +";");
        dbManager.closeDatabase();
    }
    public boolean exists() {
        SQLiteDatabase db = dbManager.openDatabase();
        if (this.hasProperty(DBRefs.Columns.ID)) {
            String query = "SELECT * FROM " + getTable() + " WHERE " + DBRefs.Columns.ID + " = '" + this.get_id() + "';";
            Cursor c = db.rawQuery(query, null);
            if (c.getCount() <= 0) {
                c.close();
            } else {
                c.close();
                dbManager.closeDatabase();
                return true;
            }
        }
        String query = "SELECT * FROM " + getTable() + " WHERE " + getWhereSQL() + ";";
        Cursor c = db.rawQuery(query, null);
        if (c.getCount() <= 0) {
            c.close();
            dbManager.closeDatabase();
            return false;
        }
        c.close();
        dbManager.closeDatabase();
        return true;
    }
    public int count() {
        DBManager dbManager = DBManager.getInstance(new DBHandler());
        SQLiteDatabase db = dbManager.openDatabase();

        Cursor c = db.rawQuery("SELECT " + DBRefs.Columns.ID + " FROM " + getTable(), null);
        int count = c.getCount();

        c.close();
        dbManager.closeDatabase();
        return count;
    }
    public T get() {
        SQLiteDatabase db = dbManager.openDatabase();
        try {
            String query;
            if (this.hasProperty(DBRefs.Columns.ID) && this.exists())
                query = "SELECT * FROM " + getTable() + " WHERE " + getIDWhereSQL() + ";";
            else
                query = "SELECT * FROM " + getTable() + " WHERE " + getWhereSQL() + ";";
            Log.v(CRRefs.TAG, "[DBObject.get]: " + verbose() + " ::: Query: " + query);
            Cursor c = db.rawQuery(query, null);

            // Move to first entry, if success; get and return it, if false; return the default
            if (c.moveToFirst()) {
                this.setValuesFromCursor(c);
                Log.d(CRRefs.TAG, "[DBObject.get]: SUCCESS - " + verbose() + " ::: " + query);
                try {
                    T ret = (T) this;
                    c.close();
                    dbManager.closeDatabase();
                    return ret;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dbManager.closeDatabase();
        return (T) this;
    }
    public ArrayList<Integer> getWhere(String whereSQL) {
        SQLiteDatabase db = dbManager.openDatabase();

        // The ArrayList to contain all objects before returned
        ArrayList<Integer> list = new ArrayList<>();

        String query = "SELECT * FROM " + getTable() + (whereSQL == null ? "" : " WHERE " + whereSQL) + ";";
        Log.v(CRRefs.TAG, "[DBObject.getWhere]: " + verbose() + " ::: Query: " + query);
        Cursor c = db.rawQuery(query, null);
        Log.v(CRRefs.TAG, "[DBObject.getWhere] Returned Count: " + c.getCount());
        if (c.moveToFirst()) {
            do {
                int i = CursorUtils.getInt(c, DBRefs.Columns.ID);
                list.add(i);
                Log.v(CRRefs.TAG, "[DBObject.getWhere] New entry added: " + i);
            } while (c.moveToNext());
        }

        c.close();
        dbManager.closeDatabase();
        return list;
    }
    public ArrayList<Integer> getAll() {
        SQLiteDatabase db = dbManager.openDatabase();

        // The ArrayList to contain all objects before returned
        ArrayList<Integer> list = new ArrayList<>();

        String query = "SELECT * FROM " + getTable() + ";";
        Log.v(CRRefs.TAG, "[DBObject.getAll]: " + verbose() + " ::: Query: " + query);
        Cursor c = db.rawQuery(query, null);
        Log.v(CRRefs.TAG, "[DBObject.getAll] Returned Count: " + c.getCount());
        if (c.moveToFirst()) {
            do {
                int i = CursorUtils.getInt(c, DBRefs.Columns.ID);
                list.add(i);
                Log.v(CRRefs.TAG, "[DBObject.getAll] New entry added: " + i);
            } while (c.moveToNext());
        }

        c.close();
        dbManager.closeDatabase();
        return list;
    }

    // ####################
    // ###   ToString   ###
    // ####################
    @Override
    public String toString() {
        return values.toString();
    }
    public String verbose() {
        return getTable() + " | " + this.values.toString();
    }
}
