package no.speiderapp.android.speiderapp.reference;

/*
 * Extra References
 */
public class EXRefs {
    public static final String ACHIEVEMENT_ID = "ACHIEVEMENT_ID";

    @Deprecated
    public static final String FIRST_TIME_SETUP = "FIRST_TIME_SETUP";

    @Deprecated
    public static final String BADGE_KEY = "BADGE_KEY";
}
