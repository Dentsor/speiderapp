package no.speiderapp.android.speiderapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.DBRefs;

public class DBHandler extends SQLiteOpenHelper {

    public DBHandler() {
        this(null, null);
    }

    public DBHandler(Context context) {
        this(context, null);
    }

    public DBHandler(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DBRefs.DATABASE_NAME, factory, DBRefs.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v(CRRefs.TAG, "[DBHandler.onCreate]: Creating database");
        buildDatabase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v(CRRefs.TAG, "[DBHandler.onUpgrade]: Upgrading database! Old: " + oldVersion + " - New: " + newVersion);

        // These are automatically created and based on an online database, therefore safe to delete
        purgeCacheTables(db);

        // Commented out due to danger of deleting user data
        //purgeSensitiveTables(db);

        onCreate(db);
    }

    public void buildDatabase(SQLiteDatabase db) {
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Started building database");

        // Create Achievement Table
        String queryAchievements = "CREATE TABLE IF NOT EXISTS " + DBRefs.Tables.ACHIEVEMENTS + " ("
                + DBRefs.ColumnsAchievements.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRefs.ColumnsAchievements.ASSOCIATION + " INTEGER, "
                + DBRefs.ColumnsAchievements.DESCRIPTION + " TEXT, "
                + DBRefs.ColumnsAchievements.AUDIENCE + " INTEGER, "
                + DBRefs.ColumnsAchievements.OPTIONAL + " INTEGER, "
                + DBRefs.ColumnsAchievements.OPTIONALS + " INTEGER, "
                + DBRefs.ColumnsAchievements.LINK + " INTEGER, "
                + DBRefs.ColumnsAchievements.PARENT + " INTEGER, "
                + DBRefs.ColumnsAchievements.DISPLAY + " INTEGER, "
                + DBRefs.ColumnsAchievements.TYPE + " INTEGER, "
                + DBRefs.ColumnsAchievements.VERSION + " INTEGER "
                + ");";
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Building table: " + queryAchievements);
        db.execSQL(queryAchievements);

        // Create Progression Table
        String queryProgression = "CREATE TABLE IF NOT EXISTS " + DBRefs.Tables.PROGRESSION + " ("
                + DBRefs.ColumnsProgression.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRefs.ColumnsProgression.ACHID + " INTEGER, "
                + DBRefs.ColumnsProgression.PROFILE + " INTEGER, "
                + DBRefs.ColumnsProgression.PROGRESS + " INTEGER, "
                + DBRefs.ColumnsProgression.LAST_CHANGED + " INTEGER, "
                + DBRefs.ColumnsProgression.LAST_SYNCED + " INTEGER "
                + ");";
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Building table: " + queryProgression);
        db.execSQL(queryProgression);

        // Create Profiles Table
        String queryProfiles = "CREATE TABLE IF NOT EXISTS " + DBRefs.Tables.PROFILES + " ("
                + DBRefs.ColumnsProfiles.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRefs.ColumnsProfiles.NAME + " TEXT "
                + ");";
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Building table: " + queryProfiles);
        db.execSQL(queryProfiles);

        String queryAssociations = "CREATE TABLE IF NOT EXISTS " + DBRefs.Tables.ASSOCIATIONS + " ("
                + DBRefs.ColumnsAssociations.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRefs.ColumnsAssociations.NAME + " TEXT, "
                + DBRefs.ColumnsAssociations.ABBREVIATION + " TEXT, "
                + DBRefs.ColumnsAssociations.COUNTRY + " TEXT "
                + ");";
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Building table: " + queryAssociations);
        db.execSQL(queryAssociations);

        String queryAudiences = "CREATE TABLE IF NOT EXISTS " + DBRefs.Tables.AUDIENCES + " ("
                + DBRefs.ColumnsAudiences.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBRefs.ColumnsAudiences.NAME + " TEXT, "
                + DBRefs.ColumnsAudiences.AGE_GROUP + " TEXT, "
                + DBRefs.ColumnsAudiences.ASSOCIATION + " INTEGER, "
                + DBRefs.ColumnsAudiences.DISPLAY_ORDER + " INTEGER "
                + ");";
        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Building table: " + queryAudiences);
        db.execSQL(queryAudiences);

        Log.v(CRRefs.TAG, "[DBHandler.buildDatabase]: Completed building database");
    }
    public void purgeDatabase(SQLiteDatabase db) {
        purgeCacheTables(db);
        purgeSensitiveTables(db);
    }

    private void purgeTable(SQLiteDatabase db, String table) {
        Log.v(CRRefs.TAG, "[DBHandler.purgeTable]: Purging " + table);
        db.execSQL("DROP TABLE IF EXISTS " + table);
    }

    private void purgeCacheTables(SQLiteDatabase db) {
        Log.v(CRRefs.TAG, "[DBHandler.purgeCacheTables]: Started purging 'cached tables'");

        // These are automatically created and based on an online database, therefore safe to delete
        purgeTable(db, DBRefs.Tables.ACHIEVEMENTS);
        purgeTable(db, DBRefs.Tables.ASSOCIATIONS);
        purgeTable(db, DBRefs.Tables.AUDIENCES);

        Log.v(CRRefs.TAG, "[DBHandler.purgeCacheTables]: Completed purging 'cached tables'");
    }
    private void purgeSensitiveTables(SQLiteDatabase db) {
        Log.v(CRRefs.TAG, "[DBHandler.purgeSensitiveTables]: Started purging 'sensitive tables'");

        // These are "sensitive" due to danger of deleting user data
        purgeTable(db, DBRefs.Tables.PROGRESSION);
        purgeTable(db, DBRefs.Tables.PROFILES);

        Log.v(CRRefs.TAG, "[DBHandler.purgeSensitiveTables]: Completed purging 'sensitive tables'");
    }
}
