package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;

import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.reference.SPRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public class DBAchievement extends DBObject<DBAchievement> {

    private DBAchievement() {
        super(new ContentValues());
    }
    public DBAchievement(int id) {
        this();
        set_id(id);
    }
    public DBAchievement(int association, int audience) {
        this();
        set_association(association);
        set_audience(audience);
    }
    public DBAchievement(int id, int association, String description, int audience, boolean optional, int optionals, int link, int parent, int display, int type, int version) {
        this();
        set_id(id);
        set_association(association);
        set_description(description);
        set_audience(audience);
        set_optional(optional);
        set_optionals(optionals);
        set_link(link);
        set_parent(parent);
        set_display(display);
        set_type(type);
        set_version(version);
    }

    @Override
    protected String getTable() {
        return DBRefs.Tables.ACHIEVEMENTS;
    }

    @Override
    public String getWhereSQL() {
        return DBRefs.ColumnsAchievements.ID + " = '" + this.get_id() + "'";
    }

    @Override
    public void setValuesFromCursor(Cursor cursor) {
        set_id(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.ID));
        set_association(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.ASSOCIATION));
        set_description(CursorUtils.getString(cursor, DBRefs.ColumnsAchievements.DESCRIPTION));
        set_audience(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.AUDIENCE));
        set_optional(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.OPTIONAL) == 1);
        set_optionals(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.OPTIONALS));
        set_link(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.LINK));
        set_parent(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.PARENT));
        set_display(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.DISPLAY));
        set_type(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.TYPE));
        set_version(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.VERSION));
    }

    // Getters
    public int get_association() {
        return getAsInteger(DBRefs.ColumnsAchievements.ASSOCIATION);
    }
    public String get_description() {
        return getAsString(DBRefs.ColumnsAchievements.DESCRIPTION);
    }
    public int get_audience() {
        return getAsInteger(DBRefs.ColumnsAchievements.AUDIENCE);
    }
    public boolean is_optional() {
        return getAsInteger(DBRefs.ColumnsAchievements.OPTIONAL) == 1;
    }
    public int get_optionals() {
        return getAsInteger(DBRefs.ColumnsAchievements.OPTIONALS);
    }
    public int get_link() {
        return getAsInteger(DBRefs.ColumnsAchievements.LINK);
    }
    public int get_parent() {
        return getAsInteger(DBRefs.ColumnsAchievements.PARENT);
    }
    public int get_display() {
        return getAsInteger(DBRefs.ColumnsAchievements.DISPLAY);
    }
    public int get_type() {
        return getAsInteger(DBRefs.ColumnsAchievements.TYPE);
    }
    public int get_version() {
        return getAsInteger(DBRefs.ColumnsAchievements.VERSION);
    }

    public int get_progress(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        int profile = settings.getInt(SPRefs.ACTIVE_PROFILE_KEY, SPRefs.ACTIVE_PROFILE_DEFAULT);
        DBProgress prog = new DBProgress(get_id(), profile).get();
        return prog.get_progress();
    }
    public boolean is_completed(Context context) {
        return get_progress(context) == 100;
    }

    public ArrayList<Integer> getAchievements() {
        String whereSQL = DBRefs.ColumnsAchievements.ASSOCIATION + " = '" + this.get_association() + "'"
                + " AND " + DBRefs.ColumnsAchievements.AUDIENCE + " = '" + this.get_audience() + "'";

        return this.getWhere(whereSQL);
    }
    public ArrayList<Integer> getChildren() {
        String whereSQL = DBRefs.ColumnsAchievements.PARENT + " = '" + this.get_id() + "'";

        return this.getWhere(whereSQL);
    }

    // Setters
    public void set_association(int value) {
        put(DBRefs.ColumnsAchievements.ASSOCIATION, value);
    }
    public void set_description(String value) {
        put(DBRefs.ColumnsAchievements.DESCRIPTION, value);
    }
    public void set_audience(int value) {
        put(DBRefs.ColumnsAchievements.AUDIENCE, value);
    }
    public void set_optional(boolean value) {
        put(DBRefs.ColumnsAchievements.OPTIONAL, value ? 1 : 0);
    }
    public void set_optionals(int value) {
        put(DBRefs.ColumnsAchievements.OPTIONALS, value);
    }
    public void set_link(int value) {
        put(DBRefs.ColumnsAchievements.LINK, value);
    }
    public void set_parent(int value) {
        put(DBRefs.ColumnsAchievements.PARENT, value);
    }
    public void set_display(int value) {
        put(DBRefs.ColumnsAchievements.DISPLAY, value);
    }
    public void set_type(int value) {
        put(DBRefs.ColumnsAchievements.TYPE, value);
    }
    public void set_version(int value) {
        put(DBRefs.ColumnsAchievements.VERSION, value);
    }

    public void set_progress(Context context, int value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        int profile = settings.getInt(SPRefs.ACTIVE_PROFILE_KEY, SPRefs.ACTIVE_PROFILE_DEFAULT);
        DBProgress prog = new DBProgress(get_id(), profile).get();
        prog.set_progress(value);
        prog.save();
    }
    public void set_complete(Context context, boolean values) {
        set_progress(context, values ? 100 : 0);
    }

    // Misc methods
    public int calculateProgress(Context context) {
        ArrayList<Integer> children_ids = this.getChildren();

        // If this item has children, run calculateProgress, else skip (the value is set by the user and there is nothing to calculate)
        if (children_ids.size() > 0) {
            int optionalsRequired = this.get_optionals();
            int c_mandatory = 0, c_optionalsCompleted = 0, c_mandatoryCompleted = 0;

            for (int child_id : children_ids) {
                DBAchievement child = new DBAchievement(child_id).get();
                child.calculateProgress(context);

                if (!child.is_optional()) {
                    c_mandatory++;
                    if (child.is_completed(context)) {
                        c_mandatoryCompleted++;
                    }
                } else if (child.is_completed(context)) {
                    c_optionalsCompleted++;
                }
            }

            // How many is required for 100% completion
            double required = c_mandatory + optionalsRequired;

            // How many requirements has been completed (maxing optionals at the required amount)
            double completed = c_mandatoryCompleted + (c_optionalsCompleted >= optionalsRequired ? optionalsRequired : c_optionalsCompleted);

            // Divide completed by required and multiply by 100 to get percent
            int progress = (int) Math.round((completed / required) * 100);

            Log.e(CRRefs.TAG, "TEMP: [DBAchivement.calculateProgress] c_mandatory | optionalsRequired | c_mandatoryCompleted | c_optionalsCompleted : " + c_mandatory + " | " + optionalsRequired + " | " + c_mandatoryCompleted + " | " + c_optionalsCompleted);
            Log.e(CRRefs.TAG, "TEMP: [DBAchivement.calculateProgress] Required | Completed | Progress : " + required + " | " + completed + " | " + progress);

            this.set_progress(context, progress);
        }
        return this.get_progress(context);
    }
}
