package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.database.Cursor;

import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public class DBProgress extends DBObject<DBProgress> {
    private DBProgress() {
        super(new ContentValues());
    }
    public DBProgress(int id) {
        this();
        set_id(id);
    }
    // Use the following for $profile, until the feature has been properly implemented
    // PreferenceManager.getDefaultSharedPreferences(context).getInt(SPRefs.ACTIVE_PROFILE_KEY, SPRefs.ACTIVE_PROFILE_DEFAULT);
    public DBProgress(int achievementID, int profile) {
        this();
        set_achid(achievementID);
        set_profile(profile);
    }
    public DBProgress(int achievementID, int profile, int progress) {
        this();
        set_achid(achievementID);
        set_profile(profile);
        set_progress(progress);
    }

    @Override
    protected String getTable() {
        return DBRefs.Tables.PROGRESSION;
    }

    @Override
    public String getWhereSQL() {
        return DBRefs.ColumnsProgression.ACHID + " = '" + this.get_achid() + "'"
                + " AND " + DBRefs.ColumnsProgression.PROFILE + " = '" + this.get_profile() + "'";
    }

    @Override
    protected void setValuesFromCursor(Cursor cursor) {
        set_id(CursorUtils.getInt(cursor, DBRefs.ColumnsProgression.ID));
        set_achid(CursorUtils.getInt(cursor, DBRefs.ColumnsProgression.ACHID));
        set_profile(CursorUtils.getInt(cursor, DBRefs.ColumnsProgression.PROFILE));
        set_progress(CursorUtils.getInt(cursor, DBRefs.ColumnsProgression.PROGRESS));
    }

    // Getters
    public int get_achid() {
        return getAsInteger(DBRefs.ColumnsProgression.ACHID);
    }
    public int get_profile() {
        return getAsInteger(DBRefs.ColumnsProgression.PROFILE);
    }
    public int get_progress() {
        return getAsInteger(DBRefs.ColumnsProgression.PROGRESS);
    }

    // Setters
    public void set_achid(int value) {
        put(DBRefs.ColumnsProgression.ACHID, value);
    }
    public void set_profile(int value) {
        put(DBRefs.ColumnsProgression.PROFILE, value);
    }
    public void set_progress(int value) {
        put(DBRefs.ColumnsProgression.PROGRESS, value);
    }
}
