package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.database.Cursor;

import no.speiderapp.android.speiderapp.reference.CRRefs;
import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public class DBProfile extends DBObject<DBProfile> {

    private DBProfile() {
        super(new ContentValues());
    }
    public DBProfile(int id) {
        this();
        set_id(id);
    }
    public DBProfile(String name) {
        this();
        set_name(name);
    }

    @Override
    protected String getTable() {
        return DBRefs.Tables.PROFILES;
    }

    @Override
    public String getWhereSQL() {
        return DBRefs.ColumnsProfiles.NAME + " = '" + this.get_name() + "'";
    }

    @Override
    public void setValuesFromCursor(Cursor cursor) {
        set_id(CursorUtils.getInt(cursor, DBRefs.ColumnsProfiles.ID));
        set_name(CursorUtils.getString(cursor, DBRefs.ColumnsProfiles.NAME));
    }

    // Getters
    public String get_name() {
        return getAsString(DBRefs.ColumnsProfiles.NAME);
    }

    // Setters
    public void set_name(String value) {
        put(DBRefs.ColumnsProfiles.NAME, value);
    }
}
