package no.speiderapp.android.speiderapp.objects;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

import no.speiderapp.android.speiderapp.reference.DBRefs;
import no.speiderapp.android.speiderapp.utilities.CursorUtils;

public class DBAssociation extends DBObject<DBAssociation> {
    private DBAssociation() {
        super(new ContentValues());
    }
    public DBAssociation(int id) {
        this();
        set_id(id);
    }
    public DBAssociation(int id, String name, String abbreviation, String country) {
        this();
        set_id(id);
        set_name(name);
        set_abbreviation(abbreviation);
        set_country(country);
    }

    @Override
    protected String getTable() {
        return DBRefs.Tables.ASSOCIATIONS;
    }

    @Override
    public String getWhereSQL() {
        return DBRefs.ColumnsAssociations.ID + " = '" + this.get_id() + "'";
    }

    @Override
    protected void setValuesFromCursor(Cursor cursor) {
        set_id(CursorUtils.getInt(cursor, DBRefs.ColumnsAchievements.ID));
        set_name(CursorUtils.getString(cursor, DBRefs.ColumnsAssociations.NAME));
        set_abbreviation(CursorUtils.getString(cursor, DBRefs.ColumnsAssociations.ABBREVIATION));
        set_country(CursorUtils.getString(cursor, DBRefs.ColumnsAssociations.COUNTRY));
    }


    // Getters
    public String get_name() {
        return getAsString(DBRefs.ColumnsAssociations.NAME);
    }
    public String get_abbreviation() {
        return getAsString(DBRefs.ColumnsAssociations.ABBREVIATION);
    }
    public String get_country() {
        return getAsString(DBRefs.ColumnsAssociations.COUNTRY);
    }

    public ArrayList<String>[] getEntries() {
        ArrayList<Integer> assoc_ids = new DBAssociation(0).getAll();
        ArrayList<DBAssociation> assocs = new ArrayList<>();
        for (Integer id : assoc_ids) {
            assocs.add(new DBAssociation(id).get());
        }
        // Order here if wanted (see DBAudience.getEntries)
        ArrayList<String> assoc_keys = new ArrayList<>();
        ArrayList<String> assoc_vals = new ArrayList<>();
        for (DBAssociation assoc : assocs) {
            assoc_keys.add(Integer.toString(assoc.get_id()));
            assoc_vals.add(assoc.get_name());
        }
        return new ArrayList[] {assoc_keys, assoc_vals};
    }


    // Setters
    public void set_name(String value) {
        put(DBRefs.ColumnsAssociations.NAME, value);
    }
    public void set_abbreviation(String value) {
        put(DBRefs.ColumnsAssociations.ABBREVIATION, value);
    }
    public void set_country(String value) {
        put(DBRefs.ColumnsAssociations.COUNTRY, value);
    }
}
