/*
 * Based on code retrieved on 03.12.2016 22.03
 *  Source: http://www.tomswebdesign.net/Articles/Android/list-selector-dialog-class.html
 */

package no.speiderapp.android.speiderapp.utilities.External;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;

public class ListSelectorDialog {
    private Context context;
    private Builder adb;
    private String title;
    private Boolean cancelable;

    // our interface so we can return the selected key/item pair.
    public interface listSelectorInterface {
        void selectedItem(String key, String item);
        void selectorCanceled();
    }

    public ListSelectorDialog(Context c) {
        this.context = c;
        this.cancelable = true;
    }
    public ListSelectorDialog(Context c, String newTitle) {
        this(c);
        this.title = newTitle;
    }

    public ListSelectorDialog setTitle(String newTitle) {
        this.title = newTitle;
        return this;
    }
    public ListSelectorDialog setCancelable(Boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public ListSelectorDialog show(ArrayList il, final listSelectorInterface di) {
        String l[] = (String[]) il.toArray(new String[il.size()]);

        show(l, l, di);
        return this;
    }

    public ListSelectorDialog show(ArrayList il, ArrayList ik,
                            final listSelectorInterface di) {
        // convert the ArrayList's to standard Java arrays.
        String l[] = (String[]) il.toArray(new String[il.size()]);
        String k[] = (String[]) ik.toArray(new String[ik.size()]);

        show(l, k, di);
        return this;
    }

    public ListSelectorDialog show(HashMap hashmap, final listSelectorInterface di) {
        // convert the hashmap to lists
        String[] il = new String[hashmap.size()];
        String[] ik = new String[hashmap.size()];
        // HashMap iteration
        int i = 0;
        for (Object key: hashmap.keySet()) {
            il[i] = key.toString();
            ik[i] = hashmap.get(key).toString();
            i++;
        }
        // now show the selection dialog
        show(il, ik, di);
        return this;
    }

    public ListSelectorDialog show(final String[] itemList, final listSelectorInterface di) {
        // if only 1 list supplied, the list serves as both keys and values.
        show(itemList, itemList, di);
        return this;
    }

    private ListSelectorDialog show(final String[] itemList, final String[] keyList,
                            final listSelectorInterface di) {
        // set up the dialog
        adb = new AlertDialog.Builder(context);
        adb.setCancelable(false);
        adb.setItems(itemList, new DialogInterface.OnClickListener() {
            // when an item is clicked, notify our interface
            public void onClick(DialogInterface d, int n) {
                d.dismiss();
                di.selectedItem(keyList[n], itemList[n]);
            }
        });
        if (cancelable) {
            adb.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                // when user clicks cancel, notify our interface
                public void onClick(DialogInterface d, int n) {
                    d.dismiss();
                    di.selectorCanceled();
                }
            });
        }
        adb.setTitle(title);
        // show the dialog
        adb.show();
        return this;
    }
}
